# Advanced Macros for the CA65 Assembler

I've not seen much about how to use macros in the CA65 assembler outside of the online manual and a few examples posted on message boards. The macro langauge is capable of doing a lot more than the casual user likely realizes. Many people who use macros with CA65 are probably happy with a few basic macros, and have no need to explore further. In such a case, this is probably not going to be that helpful, but it may still be interesting, and there may be a few useful things to learn.

_This document assumes the reader has a basic understanding of how to use CA65 and its macros. This knowledge could be gained by reading the [CA65 Manual](https://cc65.github.io/doc/ca65.html)._

## Where to Start
### Tokens
To take full advantage of the macro language of CA65, some understanding of how the assembler works is helpful. Information from source files are processed by the assembler as tokens. All source file text is read and matched to tokens before deciding what actions to take. Knowing what these tokens are can help greatly when writing macros.

##### Some Tokens in CA65:

    a
    x
    y
    z
    s
    :=
    :++ or :-- 
    =
    <>
    <
    >
    <=
    >= 
    .and or &&
    .or or ||
    .xor or ^
    .not or ! 
    +
    -
    *
    /
    |
    ^
    &
    <<
    >>
    ~ 
    $ ( if enabled )
    ::
    .
    ,
    #
    :
    (
    )
    [
    ]
    {
    }
    @
    z:
    a:
    f:

This list doesn't include numbers, identifiers, and strings, or the many of dot keywords like `.LEFT` or `.SEGMENT`. These are also matched as single tokens.  While identifiers are case sensitive by default, the characters in these tokens are not.
Note that `:` is a token, but so is `:=` as well as `::`. Another example: `a` and `a:` are their own tokens. (As well as `A` and `A:`) If your macro code is going to be evaluating tokens, you have to watch for these pitfalls.
For more information about valid tokens, examine the files `tokens.h` and `scanner.c` in the [Github repository](https://github.com/cc65/cc65/tree/master/src/ca65). 

## A Simple Macro Example
##### _Example 1_
```
.macro loadAX oper
   .if .xmatch( .left(1, oper), #)
      lda #<(.right(.tcount(oper) - 1, oper))
      ldx #>(.right(.tcount(oper) - 1, oper))
   .else
      lda <(oper)
      ldx >(oper)
   .endif
.endmacro
```
In this example, we have a macro that loads a 16-bit value into registers A and X. It checks for an immediate operand symbol and adjusts its output. While it is suitable as it is, we are going to use it as an example of possible ways to do things differently that can be useful larger macros.
A simple change that can make the code output look a little cleaner is to use a local symbol. The keyword `.local` inside a macro will define a symbol that is only visible to that macro.

##### _Example 2_
```
.macro loadAX oper
   .local value
   .if .xmatch( .left(1, oper), #)
      value = .right(.tcount(oper) - 1, oper) ; becomes a math. expression that is evaluated
      lda #<value
      ldx #>value
   .else
      lda <(oper)	; brackets to ensure the entire expression is evaluated first
      ldx >(oper)	
   .endif
.endmacro
```
With this, the macro only uses `.right` and `.tcount` once, and it looks a bit nicer. This should always work as long as there is a valid numerical expression that can be resolved by the assembler or linker after the `#` symbol. It is still unfortunate that there are two sections for code output though. Using a `.define` can help make this macro look a bit cleaner.

# Using `.define`

The 'C-Style' defines in CA65 are sometimes initially confusing. The key thing to note is,  once a symbol is used to create a `.define`, it will always be replaced by whatever it was defined as:

```
.define plus +
foo = 3 plus 4
```
This will work because 'plus' is now replaced by the define before anything else happens with the statement evaluation. Sometimes it can be confusing to use `.define` as an include guard:
```
.ifndef mathEngine
.define mathEngine 
; etc..
```
This doesn't work, because 'mathEngine' resolves to white space. A possible workaround while still using `.define`:
```
TRUE = 1
FALSE = 0
.ifndef mathEngine
.define mathEngine TRUE
; etc..
 ```
 Note that `.define mathEngine 1` would not work, as it would resolve to `1` and cause an error,  as `1` is not a valid identifier. 
The only exception to the statement that symbols used as a `.define` will always be replaced by its definition is when using `.undefine` to remove the definition. In all other cases it will always be replaced. There is more to say about `.define`, but for now we will move on.

Applying this to our previous example we can do things a bit differently:

##### _Example 3_
```
.macro loadAX oper

   .if .xmatch(.left(1, oper), #)
      .define prefix #
      .define value .right(.tcount(oper) - 1, oper)
   .else
      .define prefix
      .define value oper
   .endif
    
   lda prefix < (value)
   ldx prefix > (value)
   
   ; .undefine to use again on next macro invocation:
   .undefine prefix
   .undefine value
    
.endmacro
```

Using `.define`in this way, we can have one section for code output that is valid for immediate or memory access modes. While this may not be extremely useful for this example, letting defines help with saving tokens for output later can simplify some macro tasks.  However, one must be aware that `.defines` are not local, they are very much global to the entire module that is being assembled, so you may want to make the names something that is unlikely to be used elsewhere, such as using some underscores, or adding the macro name: `value` could become `loadAX_value`.

Notice that it was mentioned that `.define` can save tokens. When a symbol created by a `.define` is invoked, it is replaced at the token level. Expressions in a `.define` statement are not evaluated, the list of tokens is saved. The tokens do not have to represent valid CA65 code or commands. This makes them extremely useful for pushing the macro language to do some things that may not be obvious at first.

_Note: While expressions are not evaluated in a `.define` statement, all constant numbers including hex. or binary numbers are saved as a token representing a numerical constant: Numbers prefixed with `$` or `%` or no prefix are just stored as a number token with a value, and the `$` or `%` will not be part of the token list._

## Using Parameters With `.define`

Symbols created with `.define` can also take parameters:
```
.define combineBytes (hb, lb) hb << 8 | lb
.word combineBytes $01, $FF
```
This will create a 16-bit value with a high byte of 01h and a low byte of FFh. Care has to be taken when using these macros, because everything after the last comma will be captured as the last parameter. (Up to a comment or end-of-line.) To avoid this issue, the last parameter needs to be enclosed in curly braces: `{}`

Example:
`.word combineBytes $01, {$FF}, combineBytes $01, {$FE}`

This behavior also causes an issue with the `loadAX` macro. Invoking the macro with a parameter with brackets such as: `loadAX ($1234)` or `loadAX #($1234)`will cause an assembler error. 

For `loadAX ($1234)` the problem is the line `.define value oper`. The parameter `oper` now has a bracket as the first token and is processed as a parameter list for the `.define`.  The parameter `$1234` is invalid. To avoid this problem we have to add an empty parameter list:
 `.define value () oper`
If you are going to use a `.define` without parameters, and there is any possibility of a definition of a `.define` starting with a bracket `(`, use an empty parameter list. Or just get in the habit of doing it always.

Immediate mode will suffer from an assembler error, but it is not the same problem. See the revised example:

##### _Example 4_
```
.macro loadAX oper

   .if .xmatch(.left(1, {oper}), #)
      .define prefix #
      .define value .right(.tcount({oper}) - 1, {oper})
   .else
      .define prefix
      .define value () oper
   .endif
    
   lda prefix < (value)
   ldx prefix > (value)

   .undefine prefix
   .undefine value
    
.endmacro
```
Because there are brackets around the `loadAX` macro parameter `oper`, it must be enclosed in curly braces to avoid conflicting with the brackets of the `.left`, `.right` and `.tcount` functions. This macro  should now work well for any reasonable values passed to it. Whenever there is any possibility of this kind of conflict, token lists should be enclosed in curly braces. If unsure, just always use curly braces.

_It is worth noting that `value` in this macro example could be setup as a local symbol and used as in [Example 2](#example-2), but the point here is to show ways to use `.define`._

_**Summary**_:  
* Always use curly braces when processing tokens of macro parameters with `.left`, `.right`, etc.
* Always use empty parameters for `.define` style macro definitions if the macro has no parameters.


